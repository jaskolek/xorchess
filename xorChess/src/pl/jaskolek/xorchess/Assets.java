package pl.jaskolek.xorchess;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class Assets {

	public static AtlasRegion fieldBgDark;
	public static AtlasRegion fieldBgLight;
	public static AtlasRegion bishopBlack;
	public static AtlasRegion bishopWhite;
	public static AtlasRegion kingBlack;
	public static AtlasRegion kingWhite;
	public static AtlasRegion knightBlack;
	public static AtlasRegion knightWhite;
	public static AtlasRegion pawnBlack;
	public static AtlasRegion pawnWhite;
	public static AtlasRegion queenWhite;
	public static AtlasRegion queenBlack;
	public static AtlasRegion rookWhite;
	public static AtlasRegion rookBlack;

	public static void init(TextureAtlas atlas) {
		fieldBgDark = atlas.findRegion("field-bg-dark");
		fieldBgLight = atlas.findRegion("field-bg-light");
		bishopBlack = atlas.findRegion("bishop-black");
		bishopWhite = atlas.findRegion("bishop-white");
		kingBlack = atlas.findRegion("king-black");
		kingWhite = atlas.findRegion("king-white");
		knightBlack = atlas.findRegion("knight-black");
		knightWhite = atlas.findRegion("knight-white");
		pawnBlack = atlas.findRegion("pawn-black");
		pawnWhite = atlas.findRegion("pawn-white");
		queenBlack = atlas.findRegion("queen-black");
		queenWhite = atlas.findRegion("queen-white");
		rookBlack = atlas.findRegion("rook-black");
		rookWhite = atlas.findRegion("rook-white");
	}

}
