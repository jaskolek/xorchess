package pl.jaskolek.xorchess;

import pl.jaskolek.xorchess.actors.Board;
import pl.jaskolek.xorchess.screens.EditorScreen;
import pl.jaskolek.xorchess.ui.BaseScreen;
import pl.jaskolek.xorchess.ui.UiApp;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class XorChessEditor extends UiApp {
	Board board;
	
	@Override
	public void create() {		
		generateAtlas();
		Assets.init( new TextureAtlas( Gdx.files.internal("data/assets.atlas") ) );

		super.create();
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {	
		super.render();
	}


	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	protected String atlasPath() {
		return "data/uiskin.atlas";
	}

	@Override
	protected String skinPath() {
		return "data/uiskin.json";
	}

	@Override
	protected void styleSkin(Skin skin, TextureAtlas atlas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected BaseScreen getFirstScreen() {
		return new EditorScreen(this, skin);
	}
	

	private void generateAtlas(){
		TexturePacker2.process("../xorChess-android/assets/raw", "../xorChess-android/assets/data", "assets.atlas");
	}
	
}
