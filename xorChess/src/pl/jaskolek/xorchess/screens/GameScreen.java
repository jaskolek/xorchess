package pl.jaskolek.xorchess.screens;

import java.util.Random;

import pl.jaskolek.xorchess.BoardFactory;
import pl.jaskolek.xorchess.actors.Board;
import pl.jaskolek.xorchess.actors.Field;
import pl.jaskolek.xorchess.ui.BaseScreen;
import pl.jaskolek.xorchess.ui.UiApp;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class GameScreen extends BaseScreen {
	protected Board board = null;

	protected Skin skin;
	
	public GameScreen(UiApp app, Skin skin) {
		super(app);
		this.skin = skin;
		setWidth(800);
		setHeight(480);
		
		setBoard(BoardFactory.randomBoard(new Random()));
		

		addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Actor target = event.getTarget();
				if (target instanceof Field) {
					Field field = (Field) target;
					if (field.hasPiece()) {
						field.getPiece().makeMove(getBoard(), field.getRow(),
								field.getCol());
					} else {

					}
				}

				// TODO Auto-generated method stub
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

	@Override
	public void onBackPress() {
		setBoard(BoardFactory.randomBoard(new Random()));
	}

	
	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		if (this.board != null) {
			mainTable.removeActor(this.board);
		}
		this.board = board;
		mainTable.addActor(board);
	}
}
