package pl.jaskolek.xorchess.screens;

import java.util.Random;

import pl.jaskolek.xorchess.BoardFactory;
import pl.jaskolek.xorchess.actors.Board;
import pl.jaskolek.xorchess.actors.Field;
import pl.jaskolek.xorchess.actors.pieces.Bishop;
import pl.jaskolek.xorchess.actors.pieces.King;
import pl.jaskolek.xorchess.actors.pieces.Knight;
import pl.jaskolek.xorchess.actors.pieces.Piece;
import pl.jaskolek.xorchess.actors.pieces.Queen;
import pl.jaskolek.xorchess.actors.pieces.Rook;
import pl.jaskolek.xorchess.ui.BaseScreen;
import pl.jaskolek.xorchess.ui.UiApp;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class EditorScreen extends BaseScreen {
	protected Board board = null;
	protected Skin skin;

	protected boolean isKeyPressed = false;
	protected int keyCode = 0;

	public EditorScreen(UiApp app, Skin skin) {
		super(app);

		this.skin = skin;
		setWidth(800);
		setHeight(480);

		setBoard(new Board(3, 3));

		addListener(new InputListener() {
			public boolean keyDown(InputEvent event, int keycode) {
				isKeyPressed = true;
				keyCode = keycode;

				switch (keyCode) {
				case Keys.UP:
					board.addRow();
					break;

				case Keys.DOWN:
					board.removeRow();
					break;

				case Keys.LEFT:
					board.removeCol();
					break;

				case Keys.RIGHT:
					board.addCol();
					break;
				}

				return super.keyDown(event, keycode);
			}

			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				isKeyPressed = false;

				return super.keyUp(event, keycode);
			}

			/**
			 * 1 - QUEEN
			 * 2 - KING
			 * 3 - KNIGHT
			 * 4 - BISHOP
			 * 5 - ROOK
			 * 
			 * D - DELETE PIECE
			 * LEFT SHIFT - CHANGE COLOR
			 * CLICK - MAKE MOVE
			 */
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Actor target = event.getTarget();
				if (target instanceof Field) {
					Field field = (Field) target;

					if (isKeyPressed) {
						Piece newPiece = null;

						switch (keyCode) {
						// from 1 to 5 - creating new piece
						case Keys.NUM_1:
							newPiece = new Queen(false);
							break;
						case Keys.NUM_2:
							newPiece = new King(false);
							break;
						case Keys.NUM_3:
							newPiece = new Knight(false);
							break;
						case Keys.NUM_4:
							newPiece = new Bishop(false);
							break;
						case Keys.NUM_5:
							newPiece = new Rook(false);
							break;
						case Keys.SHIFT_LEFT:
							// change color
							if (field.hasPiece()) {
								field.getPiece().changeColor();
							}
							break;
						case Keys.D:
							if (field.hasPiece()) {
								field.removePiece();
							}
							break;
						case Keys.S:
							//ZAPISUJEMY!
						}

						if (newPiece != null) {// setting new piece
							if (field.hasPiece()) {
								field.removePiece();
							}

							field.setPiece(newPiece);
						}
					} else {
						if (field.hasPiece()) {
							field.getPiece().makeMove(getBoard(),
									field.getRow(), field.getCol());
						}
					}
				}

				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

	@Override
	public void onBackPress() {
		setBoard(BoardFactory.randomBoard(new Random()));
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		if (this.board != null) {
			mainTable.removeActor(this.board);
		}
		this.board = board;
		mainTable.addActor(board);
	}
}
