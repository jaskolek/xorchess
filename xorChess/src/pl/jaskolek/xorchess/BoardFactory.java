package pl.jaskolek.xorchess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import pl.jaskolek.xorchess.actors.Board;
import pl.jaskolek.xorchess.actors.pieces.Bishop;
import pl.jaskolek.xorchess.actors.pieces.King;
import pl.jaskolek.xorchess.actors.pieces.Knight;
import pl.jaskolek.xorchess.actors.pieces.Piece;
import pl.jaskolek.xorchess.actors.pieces.Queen;
import pl.jaskolek.xorchess.actors.pieces.Rook;

import com.badlogic.gdx.utils.Array;

public class BoardFactory {

	public static Board randomBoard(Random random) {
		int rows = random.nextInt(5) + 3;
		int cols = rows + random.nextInt(3);
		int half = (int) rows * cols / 2;

		int piecesCount = half + (int) random.nextInt(half) / 2;
		int moves = (int) piecesCount / 2;

		return randomBoard(rows, cols, random, piecesCount, moves);
	}

	public static Board randomBoard(int rows, int cols, Random random,
			int piecesCount, int moves) {
		Array<Piece> pieces = new Array<Piece>();
		Board board = new Board(rows, cols);
		Piece piece;

		int max = rows * cols;

		// creating random pieces
		ArrayList<Integer> list = new ArrayList<Integer>(max);
		for (int i = 0; i < max; ++i) {
			list.add(i);
		}
		Collections.shuffle(list, random);

		int row, col;
		for (Integer num : list.subList(0, piecesCount)) {
			col = num % cols;
			row = num / cols;
			piece = createRandomPiece(board, random);
			pieces.add(piece);

			board.getField(row, col).setPiece(piece);
			if( moves > 0 ){
				piece.makeMove(board, row, col);
				moves--;
			}
		}

		return board;
	}

	public static Piece createRandomPiece(Board board, Random random) {
		int x = random.nextInt(4);
		switch (x) {
		case 0:
			return new King(false);
		case 1:
			return new Bishop(false);
		case 2:
			return new Knight(false);
		case 3:
			return new Queen(false);
		case 4:
			return new Rook(false);
		default:
			return null;
		}
	}
}
