package pl.jaskolek.xorchess.actors;

import pl.jaskolek.xorchess.Assets;
import pl.jaskolek.xorchess.actors.pieces.Piece;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;

public class Field extends Group {

	private int row;
	private int col;
	private Piece piece = null;

	public Field(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.draw(getTexture(), getX(), getY(), getWidth(), getHeight());

		super.draw(batch, parentAlpha);
	}

	private TextureRegion getTexture() {
		if ((row + col) % 2 == 1) {
			return Assets.fieldBgDark;
		} else {
			return Assets.fieldBgLight;
		}
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;

		piece.setWidth(getWidth());
		piece.setHeight(getHeight());

		addActor(piece);
	}

	public void removePiece(){
		removeActor(this.piece);
		this.piece = null;
	}
	
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}
	
	public boolean hasPiece(){
		return piece != null;
	}
}
