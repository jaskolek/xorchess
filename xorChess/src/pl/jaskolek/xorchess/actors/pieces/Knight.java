package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.Assets;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Knight extends ListPiece {

	private Array<Vector2> positionsList = new Array<Vector2>();

	public Knight(boolean isBlack) {
		super(isBlack);
		
		positionsList.add(new Vector2(1, 2));
		positionsList.add(new Vector2(2, 1));
		positionsList.add(new Vector2(-1, 2));
		positionsList.add(new Vector2(-2, 1));
		positionsList.add(new Vector2(1, -2));
		positionsList.add(new Vector2(2, -1));
		positionsList.add(new Vector2(-1, -2));
		positionsList.add(new Vector2(-2, -1));
	}

	@Override
	protected Array<Vector2> getPositionsList() {
		return positionsList;
	}

	@Override
	public TextureRegion getTexture() {
		if (isBlack()) {
			return Assets.knightBlack;
		} else {
			return Assets.knightWhite;
		}
	}

}
