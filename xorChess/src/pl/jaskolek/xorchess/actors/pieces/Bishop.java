package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.Assets;
import pl.jaskolek.xorchess.actors.Board;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Bishop extends LinearPiece {

	public Bishop(boolean isBlack) {
		super(isBlack);
	}

	@Override
	public TextureRegion getTexture() {
		if (isBlack()) {
			return Assets.bishopBlack;
		} else {
			return Assets.bishopWhite;
		}
	}

	@Override
	public void makeMove(Board board, int row, int col) {
		linearMove(board, row, col, -1, -1);
		linearMove(board, row, col, -1, 1);
		linearMove(board, row, col, 1, -1);
		linearMove(board, row, col, 1, 1);
	}

}
