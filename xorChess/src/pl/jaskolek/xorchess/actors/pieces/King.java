package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.Assets;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class King extends ListPiece{
	private Array<Vector2> positionsList = new Array<Vector2>();
	
	public King(boolean isBlack) {
		super(isBlack);
		
		positionsList.add(new Vector2(-1,-1) );
		positionsList.add(new Vector2(-1,1) );
		positionsList.add(new Vector2(1,1) );
		positionsList.add(new Vector2(1,-1) );
		positionsList.add(new Vector2(-1,0) );
		positionsList.add(new Vector2(1,0) );
		positionsList.add(new Vector2(0,1) );
		positionsList.add(new Vector2(0,-1) );
	}

	@Override
	protected Array<Vector2> getPositionsList() {
		return positionsList;
	}

	@Override
	public TextureRegion getTexture() {
		if (isBlack()) {
			return Assets.kingBlack;
		} else {
			return Assets.kingWhite;
		}
	}

}
