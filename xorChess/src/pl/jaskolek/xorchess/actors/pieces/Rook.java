package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.Assets;
import pl.jaskolek.xorchess.actors.Board;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Rook extends LinearPiece {

	public Rook(boolean isBlack) {
		super(isBlack);
	}

	@Override
	public TextureRegion getTexture() {
		if (isBlack()) {
			return Assets.rookBlack;
		} else {
			return Assets.rookWhite;
		}
	}

	@Override
	public void makeMove(Board board, int row, int col) {
		linearMove(board, row, col, 0, -1);
		linearMove(board, row, col, 0, 1);
		linearMove(board, row, col, -1, 0);
		linearMove(board, row, col, 1, 0);
	}

}
