package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.actors.Board;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

public abstract class Piece extends Actor {

	private boolean isBlack = true;

	public abstract TextureRegion getTexture();

	public abstract void makeMove(Board board, int row, int col);

	public Piece(boolean isBlack) {
		super();
		this.isBlack = isBlack;
		setTouchable(Touchable.disabled);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.draw(getTexture(), getX(), getY(), getWidth(), getHeight());
	}

	public void changeColor() {
		isBlack ^= true;
	}

	public boolean isBlack() {
		return isBlack;
	}

	public void setBlack(boolean isBlack) {
		this.isBlack = isBlack;
	}
}
