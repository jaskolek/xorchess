package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.actors.Board;

public abstract class LinearPiece extends Piece {

	public LinearPiece(boolean isBlack) {
		super(isBlack);
	}

	protected void linearMove(Board board, int row, int col, int vertical, int horizontal) {
		int currentRow = row;
		int currentCol = col;
		
		while(true){
			currentRow += vertical;
			currentCol += horizontal;
			
			if( !board.hasField(currentRow, currentCol ) )
				break;
			
			if( board.getField(currentRow, currentCol).hasPiece() ) {
				board.getField(currentRow, currentCol).getPiece().changeColor();
				break;
			}
		}
	}

}
