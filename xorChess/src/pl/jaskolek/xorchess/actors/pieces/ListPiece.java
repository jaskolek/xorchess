package pl.jaskolek.xorchess.actors.pieces;

import pl.jaskolek.xorchess.actors.Board;
import pl.jaskolek.xorchess.actors.Field;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class ListPiece extends Piece {

	public ListPiece(boolean isBlack) {
		super(isBlack);
	}

	protected abstract Array<Vector2> getPositionsList();
	
	@Override
	public void makeMove( Board board, int row, int col ) {
		int currentRow = row;
		int currentCol = col;
		Field field;
		
		for( Vector2 position: getPositionsList() ){
			currentRow = (int) position.x + row;
			currentCol = (int) position.y + col;
			
			if( board.hasField(currentRow, currentCol) ){
				field = board.getField(currentRow, currentCol);
				if( field.hasPiece() ){
					field.getPiece().changeColor();
				}
			}
		}
	}
}
