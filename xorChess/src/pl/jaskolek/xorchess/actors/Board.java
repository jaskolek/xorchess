package pl.jaskolek.xorchess.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class Board extends Group {
	Array<Array<Field>> fields;

	public static final int FIELD_WIDTH = 100;
	public static final int FIELD_HEIGHT = 100;

	public static final int MAX_WIDTH = 800;
	public static final int MAX_HEIGHT = 480;

	private int rows;
	private int cols;

	public Board(int rows, int cols) {
		super();
		this.rows = rows;
		this.cols = cols;

		fields = new Array<Array<Field>>(this.rows);
		for (int i = 0; i < this.rows; ++i) {
			fields.add(new Array<Field>(this.cols));
			for (int j = 0; j < this.cols; ++j) {
				Field field = new Field(i, j);
				field.setX(FIELD_WIDTH * j);
				field.setY(FIELD_HEIGHT * i);
				field.setWidth(FIELD_WIDTH);
				field.setHeight(FIELD_HEIGHT);

				fields.get(i).add(field);
				addActor(field);
			}
		}
		rescale();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Field getField(int row, int col) {
		if (!hasField(row, col))
			return null;
		return fields.get(row).get(col);
	}

	public boolean hasField(int row, int col) {
		return (row < rows && row >= 0 && col < cols && col >= 0);
	}

	public void addRow() {
		int i = this.rows;
		fields.add(new Array<Field>(this.cols));
		for (int j = 0; j < this.cols; ++j) {
			Field field = new Field(i, j);
			field.setX(FIELD_WIDTH * j);
			field.setY(FIELD_HEIGHT * i);
			field.setWidth(FIELD_WIDTH);
			field.setHeight(FIELD_HEIGHT);

			fields.get(i).add(field);
			addActor(field);
		}
		this.rows++;
		rescale();
	}

	public void addCol() {
		for (int i = 0; i < this.rows; ++i) {
			Field field = new Field(i, this.cols);
			field.setX(FIELD_WIDTH * this.cols);
			field.setY(FIELD_HEIGHT * i);
			field.setWidth(FIELD_WIDTH);
			field.setHeight(FIELD_HEIGHT);

			fields.get(i).add(field);
			addActor(field);
		}
		this.cols++;

		rescale();
	}

	public void removeCol() {
		this.cols--;

		for (int i = 0; i < this.rows; ++i) {
			removeActor(fields.get(i).get(this.cols));
			fields.get(i).removeIndex(this.cols);
		}

		rescale();
	}

	public void removeRow() {
		this.rows--;

		for (Field field : fields.get(this.rows)) {
			removeActor(field);
		}

		fields.get(this.rows).clear();
		fields.removeIndex(this.rows);
		rescale();
	}

	public void rescale() {

		setWidth(cols * FIELD_WIDTH);
		setHeight(rows * FIELD_HEIGHT);

		float scaleX = MAX_WIDTH / getWidth();
		float scaleY = MAX_HEIGHT / getHeight();

		float scale = Math.min(scaleX, scaleY);

		float scaledWidth = scale * getWidth();
		float scaledHeight = scale * getHeight();

		setScale(scale);
		setX((MAX_WIDTH - scaledWidth) / 2);
		setY((MAX_HEIGHT - scaledHeight) / 2);

	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

}
